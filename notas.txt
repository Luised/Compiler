       asm.Escribir(";||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
        asm.Escribir("pila SEGMENT PARA STACK 'STACK'");
        asm.Escribir(";NADA");
        asm.Escribir("pila ENDS");
        asm.Escribir("");
        asm.Escribir("datos SEGMENT PARA PUBLIC 'DATA'");
        ruta = "C:\\Users\\ACER\\Desktop\\Codigo\\Programa.fungi"; //ruta del archivo original..
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(ruta)));
            String linea;
            while ((linea = br.readLine()) != null) {
                linea = Metodos.clearTabs(linea);               //quita todas las tabulaciones en el programa...
                linea = Metodos.clearComments(linea);           //quita todos los comentarios que hay en la linea...
                linea = Metodos.correctionSigs(linea);        //corrige espacios antes y despues de un =
                linea = Metodos.clearSpaceAfterLetters(linea);  //quita los espacios antes de las palabras reservadas....
                linea = Metodos.clearSpaceBeforeLetters(linea);  //quita los espacios despues de la ultima palabra....
                linea = Metodos.clearMoreSpaces(linea);         //reduce los espacios a uno entre cada una de las palabreas...
                linea = Metodos.clearLineOnlyWithSpaces(linea); //elimina los espacios solos en linea y los vacios

                String s = "";
                String S = "";
                linea = linea.trim();
                if (linea.startsWith("ent")) {
                    try {
                        s = "id" + xxx++ + " db " + linea.split(" ")[3];
                    } catch (Exception e) {
                        xxx -= 1;
                        s = "id" + xxx++ + " db " + "5,?,5 DUP(?)";
                    }
                    try {
                        S = linea.split(" ")[0] + " id" + id++ + " " + linea.split(" ")[2] + " " + linea.split(" ")[3];
                    } catch (Exception e) {
                        id -= 1;
                        S = "" + linea.split(" ")[0] + " id" + id++;
                    }
                } else if (linea.startsWith("cad")) {
                    try {
                        s = "id" + xxx++ + " db \'" + linea.split(" ")[3].replace("\"", "") + "$\'";
                    } catch (Exception e) {
                        xxx -= 1;
                        s = "id" + xxx++ + " db " + "?";
                    }
                    try {
                        S = linea.split(" ")[0] + " id" + id++ + " " + linea.split(" ")[2] + " " + linea.split(" ")[3];
                    } catch (Exception e) {
                        id -= 1;
                        S = "" + linea.split(" ")[0] + " id" + id++;
                    }
                } else if (linea.startsWith("char")) {
                    s = "id" + xxx++ + " db " + "1,?,1 DUP(?)";
                } else if (linea.startsWith("boolean")) {
                    s = "id" + xxx++ + " db " + "0";
                }
                if (!s.isEmpty() & !S.isEmpty()) {
                    asm.Escribir("\t" + s);
                    a3d.Escribir(S);
                }
            }
        } catch (IOException ioe) {
            System.err.println(ioe);
        }
        a3d.Escribir("");
        asm.Escribir("datos ENDS");
        asm.Escribir("");
        asm.Escribir("extra SEGMENT PARA PUBLIC 'DATA'");
        asm.Escribir(";NADA");
        asm.Escribir("extra ENDS");
        asm.Escribir("");
        asm.Escribir("codigo SEGMENT PARA PUBLIC 'CODE'");
        asm.Escribir("PUBLIC ProcedimientoPrincipal");
        asm.Escribir("ProcedimientoPrincipal PROC FAR");
        asm.Escribir("ASSUME CS:codigo, DS:datos, SS:pila, ES:extra");
        asm.Escribir("");
        asm.Escribir(";OBLIGATORIAS");
        asm.Escribir("PUSH DS");
        asm.Escribir("MOV AX,0");
        asm.Escribir("PUSH AX");
        asm.Escribir("MOV AX, datos");
        asm.Escribir("MOV DS,AX");
        asm.Escribir("MOV AX, extra");
        asm.Escribir("MOV ES,AX");
        asm.Escribir("");
        asm.Escribir(";CODIGO");
        asm.Escribir("");
        asm.Escribir("");
        asm.Escribir(";CODIGO");
        asm.Escribir("");
        asm.Escribir("");
        asm.Escribir("RET");
        asm.Escribir("ProcedimientoPrincipal ENDP");
        asm.Escribir("");
        asm.Escribir("codigo ENDS");
        asm.Escribir("END ProcedimientoPrincipal");
        asm.Escribir(";Soy un comentario kawaii!!....");
        asm.Close();







////////////////////////////////////////////////////////////////////////////////////////













try{
            
            fw = new FileWriter(f);
            pw = new PrintWriter(fw);
            String[] s;
            
            pw.println("IMP MACRO MSJ;Define macro de impresion de cadenas");
            pw.println("    LEA DX,MSJ");
            pw.println("    MOV AH,9");
            pw.println("    INT 21H");
            pw.println("    lea dx,saltoeti");
            pw.println("    mov ah,9");
            pw.println("    int 21h");
            pw.println();
            pw.println("ENDM");
            
            pw.println("IMPID MACRO MSJ;Define macro impresion de id de cadenas");
            pw.println("    LOCAL divi1,imp1");            
            pw.println("    mov ax,MSJ");
            pw.println("    mov cx,0");
            pw.println("    mov dx,0");
            pw.println("    divi1:");
            pw.println("        div diez");
            pw.println("        push dx");
            pw.println("        mov dx,0");
            pw.println("        inc cx");
            pw.println("        cmp ax,0");
            pw.println("    jne divi1");
            pw.println("    imp1:");
            pw.println("        pop dx");
            pw.println("        add dl,30h");
            pw.println("        mov ah,2");
            pw.println("        int 21h");
            pw.println("    loop imp1");
            pw.println("    IMP saltoeti");
            pw.println("ENDM");
            pw.println();
	  
            pw.println("LECN MACRO A,B;Define macro lectura de numeros");
            pw.println("    LOCAL val1,digParcial1,obvi1,limValida1,impNoValido1,jlimValido1,saleVal1,siValido1,conversion1,brinca1");            
            pw.println("    LEA dx,A");
            pw.println("    MOV AH, 0AH");
            pw.println("    INT 21H");
            pw.println("    MOV SI,2");
            pw.println("    val1:");
            pw.println("    cmp[A+si],'0'");
            pw.println("    jl digParcial1");
            pw.println("    cmp[A+si],'9'");
            pw.println("    jg digParcial1");
            pw.println("    mov bl,30h");
            pw.println("    sub [A+si],30h");
            pw.println("    inc si");
            pw.println("    cmp [A+si],0dh");
            pw.println("    jne val1");
            pw.println("    jmp obvi1");
            pw.println("    digParcial1:");
            pw.println("    jmp fatalError");     
            pw.println("    obvi1:");            
            pw.println("    cmp A[1],5");
            pw.println("    jl jLimValido1");
            pw.println("    mov cx,5");
            pw.println("    mov si,0");
            pw.println("    mov bx,0");
            pw.println("    limValida1:");
            pw.println("    mov bl,lim[si]");
            pw.println("    mov bh,A[si+2]");
            pw.println("    cmp bh,bl");
            pw.println("    jg impNoValido1");
            pw.println("    jl siValido1");
            pw.println("    inc si");
            pw.println("    loop limValida1");            
            pw.println("    jmp saleVal1");
            pw.println("    impNoValido1:");
            pw.println("    jmp fatalError");
            pw.println("    jlimValido1:");
            pw.println("    saleVal1:");
            pw.println("    siValido1:");            
            pw.println("    mov cx,0");
            pw.println("    mov cl,A[1]");
            pw.println("    dec cl");  
            pw.println("    mov si,0");  
            pw.println("    mov ax,0");  
            pw.println("    cmp cx,0");  
            pw.println("    je brinca1");  
            pw.println("    conversion1:");  
            pw.println("    mov bl,A[si+2]");  
            pw.println("    add ax,bx");  
            pw.println("    mul diez");
            pw.println("    inc si");  
            pw.println("    loop conversion1");  
            pw.println("    cmp cx,0");              
            pw.println("    brinca1:");
            pw.println("    mov bl,A[si+2]");
            pw.println("    add ax,bx");
            pw.println("    mov B,ax");
            pw.println("    IMP saltoeti");
            pw.println("ENDM");
            pw.println();
 
            pw.println("LEC MACRO A;Define macro lectura");
            pw.println("    LEA dx,A");
            pw.println("    MOV AH, 0AH");
            pw.println("    INT 21H");
            pw.println("ENDM");
            pw.println();
        
            if(suma){
                pw.println("SUMA MACRO A,B;Define macro suma");
                pw.println("    MOV AX, A");
                pw.println("    MOV DX, B");
                pw.println("    ADD AX, DX");
                pw.println("ENDM");  
            pw.println();                              
            }
            if(resta){
                pw.println("RESTA MACRO A,B;Define macro resta");
                pw.println("    MOV AX, A");
                pw.println("    MOV DX, B");
                pw.println("    SUB AX, DX");
                pw.p