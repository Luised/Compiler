
/**
 *
 * @author lalo
 * @param FunguiCompiler
 *
 */
public class TabPalRes {

    public static final String PalabrasReservadas[] = new String[]{"ent", "cad", "char", "boolean", "imp", "leer", "si", "sino"};

    public static boolean isReservedWord(String p) {
        for (String pr : PalabrasReservadas) {
            if (pr.equals(p)) {
                return true;
            }
        }
        return false;
    }
}
