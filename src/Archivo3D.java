
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 *
 * @author LEAR
 */
public class Archivo3D {

    int n = 0;
    File f;
    String ruta;
    BufferedWriter bw;
    FileWriter fil;
    File fi;

    public Archivo3D(String ruta) {
        this.ruta = ruta;
        try {
            f = new File(ruta);
            if (!f.exists()) {  //verifica la existencia del archivo
                f.createNewFile(); //si no existe se crea
            }
            fi = f.getAbsoluteFile();
            fil = new FileWriter(fi);
            bw = new BufferedWriter(fil);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

    }

    public void Escribir(String linea) {
        try {
            if (n != 0) {
                bw.newLine();
            }
            bw.write(linea);
            n++;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

    }

    public void Close() {
        try {
            bw.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
    /*
     public static void main(String[] args) {
     Archivo3D a3d = new Archivo3D("C:\\\\Users\\\\ACER\\\\Desktop\\\\Programa.a3d");
     a3d.Escribir("Hola 1");
     a3d.Escribir("Hola 2");
     a3d.Escribir("Hola 3");
     a3d.Escribir("Hola 4");
     a3d.Escribir("Hola 5");
     a3d.Close();
     }
     */
}
