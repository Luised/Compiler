
/**
 *
 * @author lalo
 * @author eornom_eilatan
 * @param FunguiCompiler
 *
 */
import java.io.*;

public class Compilador {

    public static int temp = 0;
    public static int l = 0;
    public static int xxx = 0;
    public static int id = 0;
    public static String x;//mensaje de errores

    //public static final String a = "C:\\Users\\ACER\\Desktop"; // ACER
    public static final String a = "C:\\Documents and Settings\\Lalo\\Escritorio"; // Maquina Virtual

    public static final Archivo3D a3d = new Archivo3D(a + "\\Codigo\\Programa.a3d");
    public static final Archivo3D asm = new Archivo3D(a + "\\Codigo\\Programa.asm");
    public static String[][] let = new String[50][2];

    //*
    public static void main(String[] args) {
        //asm.Escribir("x\nx");

        String ruta; //ruta del archivo original..
        //String ruta = Archivos.OFD(); //ruta del archivo original..
        ruta = a + "\\Codigo\\Programa.fungi"; //ruta del archivo original..
        String rutac = ruta.replace(".fungi", ".fng"); //ruta del archivo del compilado...
        System.out.println("======================CODIGO========================");

        x = "\nErrores:\n";
        try {
            File f = new File(rutac);
            if (!f.exists()) {  //verifica la existencia del archivo
                f.createNewFile(); //si no existe se crea
            }

            try (BufferedWriter bw = new BufferedWriter(new FileWriter(f.getAbsoluteFile()))) {
                BufferedReader br = new BufferedReader(new FileReader(new File(ruta)));
                String linea;
                int n = 0;
                //------------------------------------------------------------------------------
                ruta = a + "\\Codigo\\Programa.fungi"; //ruta del archivo original..
                try {
                    BufferedReader brr = new BufferedReader(new FileReader(new File(ruta)));
                    while ((linea = brr.readLine()) != null) {
                        linea = Metodos.clearTabs(linea);               //quita todas las tabulaciones en el programa...
                        linea = Metodos.clearComments(linea);           //quita todos los comentarios que hay en la linea...
                        linea = Metodos.correctionSigs(linea);        //corrige espacios antes y despues de un =
                        linea = Metodos.clearSpaceAfterLetters(linea);  //quita los espacios antes de las palabras reservadas....
                        linea = Metodos.clearSpaceBeforeLetters(linea);  //quita los espacios despues de la ultima palabra....
                        linea = Metodos.clearMoreSpaces(linea);         //reduce los espacios a uno entre cada una de las palabreas...
                        linea = Metodos.clearLineOnlyWithSpaces(linea); //elimina los espacios solos en linea y los vacios

                        String S = "";
                        linea = linea.trim();
                        if (linea.startsWith("ent")) {
                            try {
                                S = linea.split(" ")[0] + " id" + id++ + " " + linea.split(" ")[2] + " " + linea.split(" ")[3];
                            } catch (Exception e) {
                                id -= 1;
                                S = "" + linea.split(" ")[0] + " id" + id++;
                            }
                        } else if (linea.startsWith("cad")) {
                            try {
                                S = linea.split(" ")[0] + " id" + id++ + " " + linea.split(" ")[2] + " " + linea.split(" ")[3];
                            } catch (Exception e) {
                                id -= 1;
                                S = "" + linea.split(" ")[0] + " id" + id++;
                            }
                        }
                        if (!S.isEmpty()) {
                            //asm.Escribir("\t" + s);
                            a3d.Escribir(S);
                        }
                    }
                } catch (IOException ioe) {
                    System.err.println(ioe);
                }
                a3d.Escribir("");
                //------------------------------------------------------------------------------
                while ((linea = br.readLine()) != null) {
                    linea = Metodos.clearTabs(linea);               //quita todas las tabulaciones en el programa...
                    linea = Metodos.clearComments(linea);           //quita todos los comentarios que hay en la linea...
                    linea = Metodos.correctionSigs(linea);        //corrige espacios antes y despues de un =
                    linea = Metodos.clearSpaceAfterLetters(linea);  //quita los espacios antes de las palabras reservadas....
                    linea = Metodos.clearSpaceBeforeLetters(linea);  //quita los espacios despues de la ultima palabra....
                    linea = Metodos.clearMoreSpaces(linea);         //reduce los espacios a uno entre cada una de las palabreas...
                    linea = Metodos.clearLineOnlyWithSpaces(linea); //elimina los espacios solos en linea y los vacios
                    ComprobacionDeLlaves.evaluaLlave(linea);
                    System.out.println((n + 1) + ":\t" + linea);

                    Metodos.analizeLine(linea, (n + 1));                     //aqui manda a analizar la linea de una x una..

                    if (n != 0) {
                        bw.newLine();
                    }
                    n++;
                    bw.write(linea);
                }
            }
        } catch (IOException ioe) {
            System.err.println(ioe);
        }
        a3d.Close();

        //          ASM
        //----------------------------------------
        ruta = a + "\\Analizador Lexico\\src\\zMacros.txt"; //ruta del archivo original..
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(ruta)));
            String linea;
            while ((linea = br.readLine()) != null) {
                asm.Escribir(linea);
            }
        } catch (IOException ioe) {
            System.err.println(ioe);
        }
        asm.Escribir(";============================================================================================================");
        asm.Escribir("pila SEGMENT PARA STACK 'STACK'");
        asm.Escribir("    db 64h dup(00h)");
        asm.Escribir("pila ENDS");
        asm.Escribir(";============================================================================================================");
        asm.Escribir("datos SEGMENT PARA PUBLIC 'DATA'");

        asm.Escribir("    lim db 6,5,5,3,5");
        asm.Escribir("    diez dw 0ah");
        asm.Escribir("    ERROR db \"ERROR... Ocurrio un error inesperado$\"");
        asm.Escribir("    lecdn db 6,?,6 dup(?)");
        asm.Escribir("    saltoeti db 10,13,\"$\"");

        ruta = a + "\\Codigo\\Programa.fungi"; //ruta del archivo original..
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(ruta)));
            String linea;
            while ((linea = br.readLine()) != null) {
                linea = Metodos.clearTabs(linea);               //quita todas las tabulaciones en el programa...
                linea = Metodos.clearComments(linea);           //quita todos los comentarios que hay en la linea...
                linea = Metodos.correctionSigs(linea);        //corrige espacios antes y despues de un =
                linea = Metodos.clearSpaceAfterLetters(linea);  //quita los espacios antes de las palabras reservadas....
                linea = Metodos.clearSpaceBeforeLetters(linea);  //quita los espacios despues de la ultima palabra....
                linea = Metodos.clearMoreSpaces(linea);         //reduce los espacios a uno entre cada una de las palabreas...
                linea = Metodos.clearLineOnlyWithSpaces(linea); //elimina los espacios solos en linea y los vacios

                String s = "";
                linea = linea.trim();
                if (linea.startsWith("ent")) {
                    try {
                        s = "id" + xxx++ + " dw " + linea.split(" ")[3];
                    } catch (Exception e) {
                        xxx -= 1;
                        s = "id" + xxx++ + " dw " + "5,?,5 DUP(?)";
                    }
                } else if (linea.startsWith("cad")) {
                    try {
                        s = "id" + xxx++ + " db \'" + linea.split(" ")[3].replace("\"", "") + "$\'";
                    } catch (Exception e) {
                        xxx -= 1;
                        s = "id" + xxx++ + " dw " + "?";
                    }
                } else if (linea.startsWith("char")) {
                    s = "id" + xxx++ + " dw " + "1,?,1 DUP(?)";
                } else if (linea.startsWith("boolean")) {
                    s = "id" + xxx++ + " dw " + "0";
                } else if (linea.startsWith("imprimir") && linea.contains("\"")) {
                    s = "l" + l + " db '" + linea.split("\"")[1] + "$'";
                    //String[50][2]
                    let[l][1] = "'" + linea.split("\"")[1] + "$'";
                    let[l][0] = "l" + l++;

                    //  k   v
                    /*//System.out.println(let);
                     for (int i = 0; i < let.length; i++) {
                     System.err.print(let[i][0] + "\t\t" + let[i][1] + "\n");
                     }
                     //*/
                }
                if (!s.isEmpty()) {
                    asm.Escribir("\t\t\t" + s);
                }
            }
            for (int i = 0; i < temp; i++) {
                asm.Escribir("\t\t\tt" + i + "  dw 0");
            }
        } catch (IOException ioe) {
            System.err.println(ioe);
        }
        asm.Escribir("datos ENDS");
        asm.Escribir(";============================================================================================================");
        asm.Escribir("extra SEGMENT PARA PUBLIC 'DATA'");
        asm.Escribir(";NADA");
        asm.Escribir("extra ENDS");
        asm.Escribir(";============================================================================================================");
        asm.Escribir("codigo SEGMENT PARA PUBLIC 'CODE'");
        asm.Escribir("PUBLIC ProcedimientoPrincipal");
        asm.Escribir("ProcedimientoPrincipal PROC FAR");
        asm.Escribir("ASSUME CS:codigo, DS:datos, SS:pila, ES:extra");
        asm.Escribir(";============================================================================================================");
        asm.Escribir(";OBLIGATORIAS");
        asm.Escribir("PUSH DS");
        asm.Escribir("MOV AX,0");
        asm.Escribir("PUSH AX");
        asm.Escribir("MOV AX, datos");
        asm.Escribir("MOV DS,AX");
        asm.Escribir("MOV AX, extra");
        asm.Escribir("MOV ES,AX");
        asm.Escribir("");
        asm.Escribir(";xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        //asm.Escribir("Limp_pant");
        String linea;
        try {
            ruta = a + "\\Codigo\\Programa.a3d";
            BufferedReader br = new BufferedReader(new FileReader(new File(ruta)));
            while ((linea = br.readLine()) != null) {
                if (linea.startsWith("imprimir")) {
                    //System.err.println(linea);
                    switch (TabVar.getType(linea.split(" ")[1])) {
                        case "ent":
                            asm.Escribir("imprimenumero " + linea.split(" ")[1]);
                            break;
                        case "cad":
                        case "char":
                            if (linea.split(" ")[1].startsWith("\"")) {
                                asm.Escribir("imprimir " + linea.split(" ")[1]);
                            } else {
                                asm.Escribir("imprimir " + linea.split(" ")[1]);
                            }
                            break;
                        default:
                            try {
                                for (String[] let1 : let) {
                                    try {
                                        String tem = let1[1];
                                        if (tem.equals("'" + linea.split("\"")[1] + "$'")) {
                                            asm.Escribir("imprimir " + let1[0]);
                                        }
                                    } catch (Exception e) {
                                    }
                                }
                            } catch (Exception e) {
                                System.err.println(e);
                            }

                        //System.out.println("imprimir id " + "'" + linea.split("\"")[1] + "$'");
                    }
                } else if (linea.startsWith("leer")) {
                    switch (TabVar.getType(linea.split(" ")[1])) {
                        case "ent":
                            asm.Escribir("leenumero lecdn," + linea.split(" ")[1]);
                            break;
                        case "cad":
                        case "char":
                            asm.Escribir("leer " + linea.split(" ")[1]);
                            break;
                    }
                } else if (linea.startsWith("id")) {
                    try {
                        asm.Escribir("MOV " + linea.split(" ")[0] + " , AX   ; asignacion de resultado");
                    } catch (Exception e) {
                    }
                } else if (linea.startsWith("t")) {
                    switch (linea.split(" ")[3]) {
                        case "*":
                            asm.Escribir("multiplicacion " + linea.split(" ")[2] + " , " + linea.split(" ")[4]);
                            asm.Escribir("\tMOV " + linea.split(" ")[0] + " , AX");
                            break;
                        case "/":
                            asm.Escribir("divicion " + linea.split(" ")[2] + " , " + linea.split(" ")[4]);
                            asm.Escribir("\tMOV " + linea.split(" ")[0] + " , AX");
                            break;
                        case "-":
                            asm.Escribir("resta " + linea.split(" ")[2] + " , " + linea.split(" ")[4]);
                            asm.Escribir("\tMOV " + linea.split(" ")[0] + " , AX");
                            break;
                        case "+":
                            asm.Escribir("suma " + linea.split(" ")[2] + " , " + linea.split(" ")[4]);
                            asm.Escribir("\tMOV " + linea.split(" ")[0] + " , AX");
                            break;
                    }
                }
            }
        } catch (Exception e) {
        }

        asm.Escribir(";xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        asm.Escribir("");
        asm.Escribir("    jmp salir");
        asm.Escribir("    erro:");
        asm.Escribir("    imprimir ERROR");
        asm.Escribir("    salir:\r\n");
        asm.Escribir("RET");
        asm.Escribir("ProcedimientoPrincipal ENDP");
        asm.Escribir("");
        asm.Escribir("codigo ENDS");
        asm.Escribir("END ProcedimientoPrincipal");
        asm.Escribir(";Soy un comentario kawaii!!....");
        asm.Close();
        //-------------------------------- 

        //Ejecuta el .bat
        execute(a + "\\Codigo\\Compilar.bat");

        //-- imprime errores..
        System.out.println("====================================================");
        ComprobacionDeLlaves.comparacion();
        if (!(x.equals("\nErrores:\n"))) {
            System.out.println(x);
        } else {
            System.out.println("El codigo no contiene errores!!...   :D");
        }

        System.out.println("=================Tabla de Simbolos:=================\nID:\tType:\tValue:\tIDsec:");
        if (TabVar.getTabVar() != null) {
            for (String[] t : TabVar.getTabVar()) {
                for (String y : t) {
                    System.out.print(y + "\t");
                }
                System.out.println("");
            }
        } else {
            System.out.println("No existen bariables....");
        }
        System.out.println("====================================================");
        // System.out.println(temp);
    }
    //*/

    static void execute(String comando) {
        try {
            String linea;
            Process p = Runtime.getRuntime().exec(comando);
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((linea = input.readLine()) != null) {
                //System.out.println(linea);
            }
            input.close();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

}
