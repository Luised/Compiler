
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author lalo
 * @param name FunguiCompiler
 * 
 */
public class Archivos {
     public static String OFD() {
        try {

            JFileChooser fc = new JFileChooser();
            fc.setFileFilter(new FileFilter() {
                @Override
                public boolean accept(File f) {
                    String fName = f.getName().toUpperCase();
                    return (fName.endsWith(".FUNGI") || f.isDirectory()) ? true : false;
                }

                public String getDescription() {
                    return "fungi File (*.FUNGI)";
                }

            });

            fc.setDialogTitle("FungiCompiler..");
            fc.setApproveButtonText("Abrir fungi");
            int returnVal = fc.showOpenDialog(null);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                //  System.out.println(fc.getCurrentDirectory());
                return fc.getSelectedFile().getAbsolutePath();
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }

    }
}
