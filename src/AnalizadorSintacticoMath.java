/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this Compilador.template file, choose Tools | Templates
 * and open the Compilador.template in the editor.
 */

import java.util.*;

/**
 *
 * @author lalo
 */
public class AnalizadorSintacticoMath {

    public static int estado = 0;
    private static int iter = 0;
    public static List<Integer> pila = new LinkedList<>();
    public static String[] cadena;

    public static List valor = new LinkedList<>();
    public static String accion = "";
    public static final String con[] = {"", "A", "A", "A", "B", "B", "B", "C", "C", "C"};
    public static final int red[] = {0, 3, 3, 1, 3, 3, 1, 3, 1, 1};
    public static final String TabMath[][] = {
        //       N     I     +     -     *    /     (     )     $     A     B     C
        /*00 */{"S5", "S6", "er", "er", "er", "er", "S4", "er", "er", "1", "2", "3"},
        /*01*/ {"er", "er", "S7", "S8", "er", "er", "er", "er", "AC", "er", "er", "er"},
        /*02*/ {"er", "er", "R3", "R3", "S9", "S10", "er", "R3", "R3", "er", "er", "er"},
        /*03*/ {"er", "er", "R6", "R6", "R6", "R6", "er", "R6", "R6", "er", "er", "er"},
        /*04*/ {"S5", "S6", "er", "er", "er", "er", "S4", "er", "er", "11", "2", "3"},
        /*05*/ {"er", "er", "R8", "R8", "R8", "R8", "er", "R8", "R8", "er", "er", "er"},
        /*06*/ {"er", "er", "R9", "R9", "R9", "R9", "er", "R9", "R9", "er", "er", "er"},
        /*07*/ {"S5", "S6", "er", "er", "er", "er", "S4", "er", "er", "er", "12", "3"},
        /*08*/ {"S5", "S6", "er", "er", "er", "er", "S4", "er", "er", "er", "13", "3"},
        /*09*/ {"S5", "S6", "er", "er", "er", "er", "S4", "er", "er", "er", "er", "14"},
        /*10*/ {"S5", "S6", "er", "er", "er", "er", "S4", "er", "er", "er", "er", "15"},
        /*11*/ {"er", "er", "S7", "S8", "er", "er", "er", "S16", "er", "er", "er", "er"},
        /*12*/ {"er", "er", "R1", "R1", "S9", "S10", "er", "R1", "R1", "er", "er", "er"},
        /*13*/ {"er", "er", "R2", "R2", "S9", "S10", "er", "R2", "R2", "er", "er", "er"},
        /*14*/ {"er", "er", "R4", "R4", "R4", "R4", "er", "R4", "R4", "er", "er", "er"},
        /*15*/ {"er", "er", "R5", "R5", "R5", "R5", "er", "R5", "R5", "er", "er", "er"},
        /*16*/ {"er", "er", "R7", "R7", "R7", "R7", "er", "R7", "R7", "er", "er", "er"},
        /*ER*/ {"er", "er", "er", "er", "er", "er", "er", "er", "er", "er", "er", "er"}};

    public static Integer IsExpMat(String cad, int lnn) {
        //String cadlect = cad;
        // System.err.println(cadlect);
        valor.clear();
        pila.clear();
        cad = AjusteID(cad);
        cad = cad.replaceAll("  ", " ");
        pila.add(0);
        //System.out.println("Cadena: " + (char) 27 + "[31m" + cad);
        cadena = cad.split(" ");
        while (cadena.length > 0) {

            accion = getAction(cadena[0]);
            if (accion.startsWith("S")) {
                //----------------------------------------------------------------
                pila.add(Integer.parseInt(accion.split("S")[1]));
                if (!cadena[0].startsWith("id")) {
                    valor.add(cadena[0]);
                    //System.out.println("add: " + cadena[0]);
                } else {
                    valor.add(TabVar.getValueId(cadena[0]));
                    //System.out.println("add: " + TabVar.getValue(cadena[0]));
                }
                cadena = Arrays.copyOfRange(cadena, 1, cadena.length);
                //----------------------------------------------------------------
            } else if (accion.startsWith("R")) {
                //----------------------------------------------------------------
                int y = Integer.parseInt(accion.split("R")[1]);
                //System.out.println("---------------" + y);
                int a = red[y];
                for (int x = 0; x < a; x++) {
                    pila.remove(pila.size() - 1);
                }
                pila.add(Integer.parseInt(getAction(con[y])));
                if (y == 1) {
                    try {
                        int A = Integer.parseInt(valor.get(valor.size() - 3).toString());
                        int B = Integer.parseInt(valor.get(valor.size() - 1).toString());
                        if (iter == 0) {
                            Compilador.a3d.Escribir("t" + Compilador.temp++ + " = " + A + " + " + B);
                            iter++;
                        } else {
                            Compilador.a3d.Escribir("t" + Compilador.temp++ + " = " + A + " + " + "t" + (Compilador.temp - 2));
                        }
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.add(A + B);
                        // System.out.println("t" + Compilador.temp + " de +");

                    } catch (Exception e) {
                        Compilador.x += "==> Linea: " + lnn + " Razon: Los datos no se pueden sumar\n";
                    }
                } else if (y == 2) {
                    try {
                        int A = Integer.parseInt(valor.get(valor.size() - 3).toString());
                        int B = Integer.parseInt(valor.get(valor.size() - 1).toString());
                        if (iter == 0) {
                            Compilador.a3d.Escribir("t" + Compilador.temp++ + " = " + A + " - " + B);
                            iter++;
                        } else {
                            Compilador.a3d.Escribir("t" + Compilador.temp++ + " = " + A + " - " + "t" + (Compilador.temp - 2));
                        }
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.add(A - B);
                        //System.out.println("t" + Compilador.temp + " de -");
                    } catch (Exception e) {
                        Compilador.x += "==> Linea: " + lnn + " Razon: Los datos no se pueden restar\n";
                    }
                } else if (y == 4) {
                    try {
                        int A = Integer.parseInt(valor.get(valor.size() - 3).toString());
                        int B = Integer.parseInt(valor.get(valor.size() - 1).toString());
                        if (iter == 0) {
                            Compilador.a3d.Escribir("t" + Compilador.temp++ + " = " + A + " * " + B);
                            iter++;
                        } else {
                            Compilador.a3d.Escribir("t" + Compilador.temp++ + " = " + A + " * " + "t" + (Compilador.temp - 2));
                        }
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.add(A * B);
                        //System.out.println("t" + Compilador.temp + " de *");
                    } catch (Exception e) {
                        Compilador.x += "==> Linea: " + lnn + " Razon: Los datos no se pueden multiplicar\n";
                    }
                } else if (y == 5) {
                    try {
                        int A = Integer.parseInt(valor.get(valor.size() - 3).toString());
                        int B = Integer.parseInt(valor.get(valor.size() - 1).toString());
                        if (B == 0) {
                            Compilador.x += "==> Linea: " + lnn + " Razon: Divición sobre 0 invalida.\n";
                            return null;
                        }
                        if (iter == 0) {
                            Compilador.a3d.Escribir("t" + Compilador.temp++ + " = " + A + " / " + B);
                            iter++;
                        } else {
                            Compilador.a3d.Escribir("t" + Compilador.temp++ + " = " + A + " / " + "t" + (Compilador.temp - 2));
                        }
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.add(Math.round(A / B));
                        //System.out.println("t" + Compilador.temp + " de /");
                    } catch (Exception e) {
                        Compilador.x += "==> Linea: " + lnn + " Razon: Los datos no se pueden dividir\n";
                        return null;
                    }
                } else if (y == 7) {
                    int A = Integer.parseInt(valor.get(valor.size() - 2).toString());
                    valor.remove(valor.size() - 1);
                    valor.remove(valor.size() - 1);
                    valor.remove(valor.size() - 1);
                    valor.add(A);
                }

                //----------------------------------------------------------------
            } else if (accion.equals("er")) {
                return null;
            } else if (accion.equals("AC")) {
                //System.out.println("" + (char) 27 + "[31mresultado: " + (Integer) valor.get(0));
                //Compilador.a3d.Escribir("Var = "+(Integer) valor.get(0));
                iter = 0;
                return (Integer) valor.get(0);
            }
            /*
             System.out.print("" + (char) 27 + "[32m" + valor + "\t\t\t" + pila + "\t\t\t" + accion + "\t\t\t");
             for (String ca : cadena) {
             System.out.print(ca + " ");
             }
             System.out.println("");//*/
        }
        return null;
    }

    public static String getAction(String l) {
        estado = pila.get(pila.size() - 1);//System.out.println(l + " " + estado);
        int s;
        s = (isNumber(l)) ? 0
                : (l.startsWith("id")) ? 1
                        : ("+".equals(l)) ? 2
                                : ("-".equals(l)) ? 3
                                        : ("*".equals(l)) ? 4
                                                : ("/".equals(l)) ? 5
                                                        : ("(".equals(l)) ? 6
                                                                : (")".equals(l)) ? 7
                                                                        : ("$".equals(l)) ? 8
                                                                                : ("A".equals(l)) ? 9
                                                                                        : ("B".equals(l)) ? 10
                                                                                                : ("C".equals(l)) ? 11 : -1;
        String r = "er";
        try {
            r = TabMath[estado][s];
        } catch (Exception e) {
            return r;
        }
        return r;
    }

    public static boolean isNumber(String l) {
        try {
            Integer.parseInt(l);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String AjusteID(String cad) {
        try {
            for (String[] x : TabVar.getTabVar()) {
                cad = cad.replaceAll(" " + x[0] + " ", " " + x[3] + " ");
            }
            cad = cad.trim();
        } catch (Exception e) {
        }

        // System.out.println(cad);
        return cad;
    }
}
